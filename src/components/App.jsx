import React from 'react';
import { hot } from 'react-hot-loader/root';
import styles from '../styles/components/App.scss';
import Logo from '../assets/images/React-icon.svg';
function App() {
    const dummy = {
        name: 'priyank'
    };
    console.log(Logo);
    return (
        <>
            <h1 className={styles.darkBlue}>
                This app is working Hot Reloaded
            </h1>
            <ul>{
                Object.values(dummy).map(
                    (p, index) => (<li key={index}>{p}</li>)
                )}
            </ul>
            <img src={Logo} alt="Logo" className={styles.reactLogo} />
        </>);
}
export default hot(App);
